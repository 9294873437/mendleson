import React from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Container, Row, Col } from 'react-bootstrap'

import logobrand from '../assets/images/logo.svg';
import bannerimage from '../assets/images/vector-smart-object.svg';
import bannershapes1 from '../assets/images/shapes/banner-shape-1.svg';
import bannershapes2 from '../assets/images/shapes/banner-shape-2.svg';
import bannershapes3 from '../assets/images/shapes/banner-shape-3.svg';
import bannershapes4 from '../assets/images/shapes/banner-shape-4.svg';
import bannershapes5 from '../assets/images/shapes/banner-shape-5.png';


const Header = () => {
  return (
    <div className='banner-section position-relative'>
        <div className='banner-shape-1'>
          <img src={bannershapes1} />
        </div>
        <div className='banner-shape-2'>
          <img src={bannershapes2} />
        </div>
        <div className='banner-shape-3'>
          <img src={bannershapes3} />
        </div>        
        <div className='banner-shape-4'>
          <img src={bannershapes5} />
        </div>
        <div className='banner-shape-4'>
          <img src={bannershapes4} />
        </div>                
        <Navbar expand="lg">
          <Container className='lg'>
            <Navbar.Brand href="javascript:void(0)" className="w-md-75">
              <img src={logobrand} />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto">
                <Nav.Link href="javascript:void(0)">About Us</Nav.Link>
                <Nav.Link href="javascript:void(0)">Services</Nav.Link>
                <Nav.Link href="javascript:void(0)">Team</Nav.Link>            
                <Nav.Link href="javascript:void(0)">Clients</Nav.Link>
                <Nav.Link href="javascript:void(0)">Contact Us</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>        
        <Container className='lg inside-banner'>
          <Row className='align-items-center h-100'>
            <Col xl="6" lg={5} className='align-self-end'>
              <img src={bannerimage} className="w-lg-75" />
            </Col>
            <Col xl="6" lg={7}>
              <div className='text-start px-lg-5'>
                <h1 className='ps-xl-5'>Mendleson Communication and Engagement</h1>
                <p className='ps-xl-5'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada sed ipsum, ut quam volutpat, tortor.</p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
  )
}

export default Header