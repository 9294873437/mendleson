import React from 'react';
import { Container, Row, Col } from 'react-bootstrap'

import facebook from '../assets/images/footer/facebook.svg';
import google from '../assets/images/footer/google.svg';
import linkedin from '../assets/images/footer/linkedin.svg';

const Footer = () => {
  return (
    <footer>
      <Container className='lg'>
        <Container>
          <Row>
            <Col md={3} sm={4}>
              <h3 className='heading'>Social</h3>
              <ul className='subheading'>
                <li>
                  <a>
                    <img src={facebook} className="me-3" />
                    Facebook
                  </a>
                </li>
                <li>
                  <a>
                    <img src={linkedin} className="me-3" />
                    Linkedin
                  </a>
                </li>
                <li>
                  <a>
                    <img src={google} className="me-3" />Google +
                  </a>
                </li>
              </ul>
            </Col>
            <Col md={3} sm={4}>
              <h3 className='heading'>Explore</h3>
              <ul className='subheading'>
                <li>
                  <a>Service</a>                  
                </li>
                <li>
                  <a>Team</a>
                </li>
                <li>
                  <a>Client</a>
                </li>
              </ul>
            </Col>
            <Col xl={3} sm={4}>
              <h3 className='heading'>Contact</h3>
              <ul className='subheading'>
                <li>
                  <a>Demo"gmail.com</a>                  
                </li>
                <li>
                  <a>Used for display</a>
                </li>
                <li>
                  <a>+00 00 000 000</a>                  
                </li>
              </ul>
            </Col>
            <Col xl={3} md={4}>
              <h3 className='heading'>Email</h3>
              <ul className='subheading'>
                <li>
                  <a>mendlesoncommunication@email.com</a>                  
                </li>
              </ul>
            </Col>
          </Row>
        </Container>
      </Container>
      <p className='xs py-4 mb-0'>© Copyright 2018 Mendleson Communication Pty Ltd </p>
    </footer>
  )  
}


export default Footer;
