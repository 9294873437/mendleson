import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'

import project1 from '../assets/images/project/our-project-1.jpg';
import project2 from '../assets/images/project/our-project-2.jpg';
import project3 from '../assets/images/project/our-project-3.jpg';
import shape4 from '../assets/images/shapes/shape-4.svg';

const OurProject = () => {
  return (
    <section className='our-projects-section position-relative'>
      <div className='shape-4'>
        <img src={shape4} />
      </div>
      <Container>
        <h2 className='text-uppercase position-relative d-inline-block heading-underline'>Our Projects</h2>                        
        <Row className='pt-4'>
          <Col sm={6} className="mb-sm-0 mb-4">
            <img src={project1} />              
          </Col>
          <Col sm={6}>
            <div className='pb-20 mb-sm-0 mb-4"'>
              <img src={project2} />                
            </div>
            <div>
              <img src={project3} />                
            </div>
          </Col>
        </Row>
      </Container>        
    </section>
  )
}

export default OurProject