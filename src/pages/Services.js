import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'

import training from '../assets/images/training-vector.svg';
import engagement from '../assets/images/engagement-vector.svg';
import consultation from '../assets/images/consultation-vector.svg';
import coominucation from '../assets/images/coominucation-vector.svg';
import facilation from '../assets/images/facilation-vector.svg';
import shape1 from '../assets/images/shapes/shape-1.svg';
import shape2 from '../assets/images/shapes/shape-2.svg';
import shape3 from '../assets/images/shapes/shape-3.svg';
import shape4 from '../assets/images/shapes/shape-4.svg';

const Services = () => {
  return (
    <div>

      <section className='services-section position-relative'>
        <div className='shape-1'>        
          <img src={shape1} />
        </div>
        <Container fluid>
          <h2 className='text-uppercase position-relative d-inline-block heading-underline'>Services</h2>                        
        </Container>
      </section>
      
      <section className='engagement-section'>
        <Container>
          <Row className="align-items-center">
            <Col md={6} className='text-end'>
              <h3 className='text-uppercase'>Engagement</h3>
              <p className='sm'>We love what we do and are driven by achieving great results for our clients. Our awards and impressive client list are
              testament to our high quality approach. We deliver value, creaKvity, results and excepKonal levels of customer service and professionalism.
              We specialise in infrastructure development, energy and natural resources.</p>
            </Col>
            <Col md={6}>
              <img src={engagement} className="w-md-75" />
            </Col>
          </Row>
        </Container>
      </section>

      <section className='communications-section position-relative'>
        <div className='shape-2'>
          <img src={shape2} />
        </div>
        <Container>
          <Row className="align-items-center">
            <Col md={6}>
              <img src={coominucation} className="w-md-75" />
            </Col>        
            <Col md={6} className='text-start mt-md-0 mt-4'>
              <h3 className='text-uppercase'>Communications</h3>
              <p className='sm'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Faucibus quam quis egestas orci. Scelerisque eu, vitae sapien,
              pellentesque et. Sit ac fames facilisis nibh faucibus. </p>            
            </Col>    
          </Row>
        </Container>
      </section>

      <section className='facilitation-section'>        
        <Container>
          <Row className="align-items-center">
            <Col md={6} className="text-end">
              <h3 className='text-uppercase'>Facilitation</h3>
              <p className='sm'>We love what we do and are driven by achieving great results for our clients. Our awards and impressive client
              list are testament to our high quality approach. We deliver value, creaKvity, results and excepKonal levels of customer service
              and professionalism. We specialise in infrastructure development, energy and natural resources.</p>
            </Col>
            <Col md={6}>
              <img src={facilation} className="w-md-75" />
            </Col>
          </Row>
        </Container>
      </section>      

      <section className='consultation-research-section position-relative'>
        <div className='shape-3'>
          <img src={shape3} />
        </div>
        <Container>
          <Row className="align-items-center">
            <Col md={6}>
              <img src={consultation} className="w-md-75" />
            </Col>
            <Col md={6} className='text-start mt-md-0 mt-4'>            
              <h3 className='text-uppercase'>Consultation and Research</h3>
              <p className='sm'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Faucibus quam quis egestas orci. Scelerisque eu, vitae sapien,
              pellentesque et. Sit ac fames facilisis nibh faucibus. </p>
            </Col>
          </Row>
        </Container>
      </section>

      <section className='traning-mentoring-section position-relative'>
        <div className='shape-4'>
          <img src={shape4} className="w-md-75" />
        </div>
        <Container>
          <Row className="align-items-center">
            <Col md={6} className='text-end'>      
              <h3 className='text-uppercase'>Traning & Mentoring</h3>
              <p className='sm'>We love what we do and are driven by achieving great results for our clients. Our awards and impressive client list are
              testament to our high quality approach. We deliver value, creaKvity, results and excepKonal levels of customer service and professionalism.
              We specialise in infrastructure development, energy and natural resources.</p>
            </Col>
            <Col md={6}>
              <img src={training} />
            </Col>
          </Row>
        </Container>
      </section>

    </div>
  )
}

export default Services