import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import aboutus from '../assets/images/about/about-us.svg';
import engagementicon from '../assets/images/about/coomunication-icon.svg';
import communicationicon from '../assets/images/about/enagagement-icon.svg';

const About = () => {
  return (
    
    <section className='about-section wow bounceIn'>        
        <Container className='lg'>
          <Row className='align-items-center'>
            <Col xl={6} lg={5} className="mb-lg-0 mb-4">
              <img src={aboutus} className="pe-lg-5 w-lg-50" />
            </Col>
            <Col xl={6} lg={7} className="text-start">
              <h2 className='text-uppercase position-relative d-inline-block heading-underline'>About us</h2>
              <p className='lg mb-md-5 mb-4 mt-2'>We love what we do and are driven by achieving great results for our clients. Our awards and impressive client list are
              testament to our high quality approach. We deliver value, creaKvity, results and excepKonal levels of customer service and professionalism.
              We specialise in infrastructure development, energy and natural resources.</p>
                <Row>
                  <Col lg={6} className="mb-lg-0 mb-md-4 mb-2">
                    <div className='pe-4'>
                      <img className='mb-md-4 mb-2' src={communicationicon} />                      
                      <h4 className='text-uppercase'>Engagement</h4>
                      <p className='lg'>We are engagement specialists, who have led projects at all levels of the IAP2 spectrum. 
                        <span className='text-uppercase'> Read More</span>
                      </p>                      
                    </div>
                  </Col>
                  <Col lg={6}>
                    <img className='mb-md-4 mb-2' src={engagementicon} />
                    <h4 className='text-uppercase'>Communications</h4>
                    <p className='lg'>We are award-winning leaders in communications and campaign management.
                      <span className='text-uppercase'> Read More</span>
                    </p>
                  </Col>
                </Row>
              </Col>
          </Row>
        </Container>
      </section>

  )
}

export default About

