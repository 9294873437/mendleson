import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'

import teamperson1 from '../assets/images/team/team-person-1.jpg';
import teamperson2 from '../assets/images/team/team-person-2.jpg';
import teamperson3 from '../assets/images/team/team-person-3.jpg';
import shape1 from '../assets/images/shapes/shape-1.svg';

const OurTeam = () => {
  return (
    <section className='our-team-section position-relative'>
      <div className='shape-1'>
        <img src={shape1} />
      </div>
      <Container>
        <h2 className='text-uppercase position-relative d-inline-block heading-underline'>Our Team</h2>                        
        <Row className='pt-3'>
          <Col md={4} className="mb-md-0 mb-4">
            <img src={teamperson1} />
            <h5 className='pt-md-5 pt-3'>Jessica D’suza</h5>
          </Col>
          <Col md={4} className="mb-md-0 mb-4">
            <img src={teamperson2} />
            <h5 className='pt-md-5 pt-3'>Johny Williams</h5>
          </Col>
          <Col md={4} className="mb-md-0 mb-4">
            <img src={teamperson3} />
            <h5 className='pt-md-5 pt-3'>Sanya R,</h5>
          </Col>
        </Row>
      </Container>
    </section>
  )
}

export default OurTeam