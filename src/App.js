import './App.css';
import Footer from './components/Footer';
import Header from './components/Header';
import About from './pages/About';
import Services from './pages/Services';
import OurTeam from './pages/OurTeam';
import OurProject from './pages/OurProject';

// Defining WOW 


function App() {
  return (
    <div className="App">
      
      {/* Header -  */}
      <Header />
      
      {/* About us -  */}
      <About />

      {/* Services -  */}
      <Services />

      {/* Our Team -  */}
      <OurTeam />

      {/* Our Project -  */}
      <OurProject />

      {/* Footer -  */}
      <Footer />      

    </div>
  );
}

export default App;
